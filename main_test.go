package main

import (
	"testing"
	"os"
)

func TestMakeDir(t *testing.T) {
	path := ".xxx_abc"
	mkdir(path)

	if _, err := os.Stat(path); err == nil {
		os.RemoveAll(path)
	} else {
		t.Error("Directory should have been created")
	}
}