# Rudimentary file server

## Usage
### Upload
    curl -F file=@README.md localhost:9090/up

### Get
    curl localhost:9090/README.md

## Docker
### Build
    docker build -t fileserve .
### Run
    docker run -p 9090:9090 --name fileserve --rm fileserve
    
# Docker registry
    docker login registry.gitlab.com
    docker build -t registry.gitlab.com/craigivy/fileserve .
    docker push registry.gitlab.com/craigivy/fileserve
    
    docker pull registry.gitlab.com/craigivy/fileserve:latest
    docker run -p 9090:9090 --name fileserve --rm registry.gitlab.com/craigivy/fileserve:latest
    