package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

const uploadDir = "./uploads"

func mkdir(path string) {

	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.Mkdir(path, os.ModePerm)
		log.Printf("Created directory %s\n", path)
	}
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {

	// the FormFile function takes in the POST input id file
	file, header, err := r.FormFile("file")

	if err != nil {
		fmt.Fprintln(w, err)
		return
	}

	defer file.Close()

	// out, err := os.Create("/tmp/uploadedfile")
	out, err := os.Create(uploadDir + "/" + header.Filename)
	if err != nil {
		fmt.Fprintf(w, "Unable to create the file for writing. Check your write access privilege")
		return
	}

	defer out.Close()

	// write the content from POST to the file
	_, err = io.Copy(out, file)
	if err != nil {
		fmt.Fprintln(w, err)
	}

	fmt.Fprintf(w, "File uploaded successfully : ")
	fmt.Fprintf(w, header.Filename)
}

func buildHttpHandler() http.Handler {
	var handler http.Handler

	handler = http.DefaultServeMux

	handler = corsHandler(handler)
	handler = cacheHandler(handler)
	handler = logHandler(handler)

	//if args.cors {
	//	handler = corsHandler(handler)
	//}
	//
	//if args.noCache {
	//	handler = cacheHandler(handler)
	//}
	//
	//if !args.quiet {
	//	handler = logHandler(handler)
	//}

	return handler
}

func logHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s\n", r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

func cacheHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.Header.Del("If-None-Match")
		r.Header.Del("If-Range")
		r.Header.Del("If-Modified-Since")

		w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
		if r.Proto == "HTTP/1.0" {
			w.Header().Set("Pragma", "no-cache")
		}

		handler.ServeHTTP(w, r)
	})
}

func corsHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var origin, method, headers string

		origin = r.Header.Get("Origin")

		if r.Method == "OPTIONS" {
			method = r.Header.Get("Access-Control-Request-Method")
			headers = r.Header.Get("Access-Control-Request-Headers")

			if len(origin) == 0 || len(method) == 0 {
				msg := fmt.Sprintf("%d %s: missing required CORS headers",
					http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
				http.Error(w, msg, http.StatusBadRequest)
				return
			}
		}

		if len(origin) > 0 {
			w.Header().Add("Vary", "Origin")
			w.Header().Set("Access-Control-Allow-Origin", origin)
			w.Header().Set("Access-Control-Allow-Credentials", "true")
		}

		if len(method) > 0 {
			w.Header().Add("Vary", "Access-Control-Request-Method")
			w.Header().Set("Access-Control-Allow-Methods", method)
		}

		if len(headers) > 0 {
			w.Header().Add("Vary", "Access-Control-Request-Headers")
			w.Header().Set("Access-Control-Allow-Headers", headers)
		}

		if r.Method != "OPTIONS" {
			handler.ServeHTTP(w, r)
		}
	})
}

func main() {
	mkdir(uploadDir)

	http.HandleFunc("/up", uploadHandler)
	http.Handle("/", http.FileServer(http.Dir(uploadDir)))
	handler := buildHttpHandler()

	port := flag.String("p", "9090", "Port address to start the file server on")
	log.Printf("File server starting on %s\n", *port)
	http.ListenAndServe(":"+*port, handler)
}
